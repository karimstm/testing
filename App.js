import React, { useEffect, useState } from "react";
import { Switch, Route } from "react-router-dom";
import Toasts from "./containers/Toasts";
import Dashboard from "./containers/Dashboard/Dashboard";
import Login from "./containers/Login/Login";
import withClearCache from "./ClearCache";
import {
  IsUserRedirect,
  PrivateRoute,
  ProtectedRotue,
} from "./helpers/PrivateRoute";
import { connect } from "react-redux";
import axios from "axios";
import axiosInstance from "./helpers/axiosInstance";
import { TYPES } from "./components/Toast";

const ClearCacheComponent = withClearCache(MainApp);

export const isEmptyValue = (value) => {
  return (
    value === undefined ||
    value === null ||
    value === NaN ||
    (typeof value === "object" && Object.keys(value).length === 0) ||
    (typeof value === "string" && value.trim().length() === 0)
  );
};

function App(props) {
  return <ClearCacheComponent {...props} />;
}

function MainApp(props) {
  const [isLoading, setIsLoading] = useState(true);
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(() => {
    const getMe = async () => {
      try {
        if (localStorage.getItem("token")) {
          setIsLoading(true);
          const res = await axiosInstance.get("/users/me");
          props.dispatch({
            type: "GET_ME",
            payload: res.data,
          });
          setIsAuthenticated(true);
          setIsLoading(false);
        } else throw new Error("Not token found");
      } catch (error) {
        setIsAuthenticated(false);
        setIsLoading(false);
      }
    };

    getMe();
  }, []);

  useEffect(() => {
    if (isEmptyValue(props.user)) {
      setIsAuthenticated(false);
    } else {
      setIsAuthenticated(true);
    }
  }, [props.user]);

  return (
    <div className="app HeightStretch">
      <Switch>
        <IsUserRedirect
          isAuthenticated={isAuthenticated}
          isLoading={isLoading}
          path="/login"
          loggedInPath="/"
          component={Login}
        />
        <ProtectedRotue
          isAuthenticated={isAuthenticated}
          isLoading={isLoading}
          path="/"
          component={Dashboard}
        />
      </Switch>
      <Toasts />
    </div>
  );
}

export default connect((state) => ({
  user: state.auth.authenticatedUser,
}))(App);
