import React from "react";
import { Redirect, Route } from "react-router";

export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      localStorage.getItem("token") ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location },
          }}
        />
      )
    }
  />
);

export const ProtectedRotue = ({
  isAuthenticated,
  isLoading,
  component: Component,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (isLoading) {
          return <div>Loading...</div>;
        }
        if (!isAuthenticated) {
          return (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location },
              }}
            />
          );
        }
        return <Component {...props} />;
      }}
    />
  );
};

export const IsUserRedirect = ({
  isAuthenticated,
  isLoading,
  component: Component,
  loggedInPath,
  children,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (isLoading) {
          return <div>Loading...</div>;
        }

        if (!isAuthenticated) {
          return <Component {...props} />;
        }
        return <Redirect to={{ pathname: loggedInPath }} />;
      }}
    />
  );
};
